import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PreloaderDirective} from './directives/preloader.directive'; 

@NgModule({
    imports:[],
    declarations:[PreloaderDirective],
    exports:[PreloaderDirective,CommonModule]
})

export class SharedModule{

}