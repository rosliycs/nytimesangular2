import { Component, Input, OnInit } from '@angular/core';
import { MostPopularService } from '../services/mostpopular.service';

@Component({
    moduleId: module.id,
    selector: 'most-popular',
    templateUrl: 'mostpopular.component.html',
    providers: [MostPopularService]
})

export class MostPopularComponent {
    mostPopular:MostPopular[];
    constructor(private mostPopularService: MostPopularService) {
        this.mostPopularService.getMostPopularNews().subscribe(mostPopularNews => {
          this.mostPopular=mostPopularNews.results;
          console.log(this.mostPopular);
        });
    }
}

interface MostPopular {
    asset_id: number;
    abstract: string;
    url: string;
}


