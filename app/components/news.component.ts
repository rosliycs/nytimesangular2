import { Component, Input } from '@angular/core';
import { NewsService } from "../services/news.service";
@Component({
  moduleId: module.id,
  selector: 'news',
  templateUrl: 'news.component.html',
  providers: [NewsService]
})

export class NewsComponent {
  docs: Docs[];
  constructor(private newsService: NewsService) {
    console.log("run ");
    this.newsService.getNews().subscribe(news => {
      console.log(news);
      this.docs = news.response.docs;
      console.log(this.docs);
    });
  }
}

interface Docs {
  _id: string;
  snippet: string;
  pub_date: string;
  lead_paragraph: string;
}
