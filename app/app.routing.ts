import { ModuleWithProviders } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';

import { NewsComponent } from './components/news.component';
import { MostPopularComponent } from './components/mostpopular.component';
import { HomeComponent } from './components/home/home.component';

export const routing: ModuleWithProviders = RouterModule.forRoot([
    {
        path: 'news',
        component: NewsComponent
    },
    {
        path: 'mostpopular',
        component: MostPopularComponent
    },
    {
        path: 'home',
        component: HomeComponent
    },

    {
        path: '',
        redirectTo: '/home',
        pathMatch: 'full'
    }
    /*  { path: '**', component: PageNotFoundComponent } */
]);