import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { Configuration } from "../core/configuration.core";
import 'rxjs/add/operator/map';


@Injectable()
export class NewsService {
    private configuration: Configuration;
    constructor(private http: Http) {
        this.configuration = new Configuration();
        console.log("News service initiliazed.");
    }

    getNews() {
        var baseUrl = this.configuration.BaseUrl + 'search/v2/articlesearch.json?api-key=' + this.configuration.ApiKey;
        console.log(baseUrl);
        return this.http.get(baseUrl).map(res => res.json());
    }
}


// interface PrintOutput {
//   (message: string): void;    // common case
//   (message: string[]): void;  // less common case
// }

// let printOut: PrintOutput = (message) => {
//   if (Array.isArray(message)) {
//     console.log(message.join(', '));
//   } else {
//     console.log(message);
//   }
// }

// printOut('hello');       // 'hello'
// printOut(['hi', 'bye']); // 'hi, bye'

