import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { Configuration } from "../core/configuration.core";
import 'rxjs/add/operator/map';


@Injectable()
export class MostPopularService {
    private configuration: Configuration;
    constructor(private http: Http) {
        console.log("service initialized.");
        this.configuration = new Configuration();
    }

    getMostPopularNews(){
        var baseUrl = this.configuration.BaseUrl + 'mostpopular/v2/mostviewed/all-sections/30.json?api-key=' + this.configuration.ApiKey;
        return this.http.get(baseUrl).map(res => res.json());
    }

}