import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { routing } from './app.routing';

import { AppComponent } from './app.component';
import { NewsComponent } from './components/news.component';
import { MostPopularComponent } from './components/mostpopular.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { CopyrightComponent } from './components/footer/copyright.component';
import {HomeComponent} from './components/home/home.component';


@NgModule({
  imports: [BrowserModule, HttpModule, routing],
  declarations: [AppComponent, HomeComponent,NewsComponent, MostPopularComponent,NavbarComponent,CopyrightComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
